# Wordles of the World<br>{WORDLE}{unite}

This is — to the best of our knowledge — the most comprehensive list of [Wordle](https://www.nytimes.com/games/wordle/)-like games and resources online, with {TOTAL} entries in {LANGS} languages ([{LANGSLEFT}](https://glottolog.org/glottolog/language) to go…).
Its goal is threefold: having fun, celebrating linguistic diversity, and exploring playful creativity.

This list is [collaborative](https://gitlab.com/rwmpelstilzchen/wordles/-/graphs/master) and is based on a number of sources:
[merge requests](https://gitlab.com/rwmpelstilzchen/wordles/-/merge_requests?scope=all&state=all), 
[other lists](#others),
entries suggested publicly (e.g. [here](https://twitter.com/JudaRonen/status/1485640320201203712) and [here](https://twitter.com/JudaRonen/status/1484274702273105921)) or privately, and
entries found on web search (on [Twitter](https://twitter.com/yeahwhyyes/status/1486040873980866562) and elsewhere). 
Code repository is available on [GitLab ![GitLab logo](gfx/code/gitlab.svg){.icon}](https://gitlab.com/rwmpelstilzchen/wordles).
The list and its code (*sans* various logos, emoji, fonts, etc. not created by this project) are in the [public domain ![public domain symbol](gfx/pd.svg){.icon}](https://en.wikipedia.org/wiki/Public_domain) under [CC0 ![CC0 logo](gfx/cc0.svg){.icon}](https://creativecommons.org/share-your-work/public-domain/cc0/).

See the [media section](#media) below for reading about multilingual and remixable Wordle-like games and for media coverage of Wordles of the World.



## {WORDLE}{lists} {#foobar}

<div class="pure-table" id="lists" markdown="1">
| | | |
| --- | --- | --- |
| [🌐](#ml) | [Multilingual](#ml) | Wordle-like games in various languages |
| [🔍](#domain) | [Domain-specific](#domain) | The target is not any word but is bound to a certain domain |
| [🃏](#twist) | [Twist](#twist) | Games with major twists that change the fundamental gameplay |
| [♻](#clones) | [Clones](#clones) | English-language reimplementations of Wordle on various platforms |
| [😶](#nonling) | [Non-linguistic](#nonling) | Games where the target is not a word but something else |
| [⚙](#misc) | [Miscellanea](#misc) | Tools and miscellanea |
| [🟨](#nonwordly) | [Non-Wordly](#nonwordly) | Related games with non-Wordle-like rules |
</div>



## {WORDLE}{boost} {#boost}

<div class="pure-table" id="participate" markdown="1">
| | |
| --- | --- |
| 📣 | Share this list with your friends. The more people it reaches, the more people may benefit from it and possibly suggest new entries. If you use Twitter, you can retweet [this tweet](https://twitter.com/JudaRonen/status/1485640320201203712/). |
| ➕ | Do you know of any entry that is absent from the list? [Contact us](#contact) (preferably by making a merge request) and we will be happy to add it 🙂. |
| 💡 | If you want to make a version for your language, you can follow [these](https://blog.mothertongues.org/wordle/) or [these instructions](https://github.com/cwackerfuss/react-wordle#how-can-i-create-a-version-in-another-language); requires basic technical know-how and a good wordlist. Consider making it [accessible](https://twitter.com/JudaRonen/status/1486992955227480071). |
| 🕸 | If you have already made a wordle in any language, linking [back here](https://rwmpelstilzchen.gitlab.io/wordles/) would be great, as it would create a network of Wordle-like games. |
</div>


## {WORDLE}{write} {#write}

<div class="pure-table" id="contact" markdown="1">
| | |
| --- | --- |
| ![GitLab logo](gfx/code/gitlab.svg){.icon} | If you want to suggest an edit on the [GitLab repository](https://gitlab.com/rwmpelstilzchen/wordles), please fork it and [make a merge request](https://gitlab.com/rwmpelstilzchen/wordles/-/merge_requests) (see [instructions](https://gitlab.com/rwmpelstilzchen/wordles#how-to-contribute)), or [open an issue](https://gitlab.com/rwmpelstilzchen/wordles/-/issues). |
| 👩‍💻 | If you don’t want to use Git, or don’t know how to, feel free to contact the coordinator — Júda Ronén — [through other ways of communication](http://me.digitalwords.net/), in Hebrew, Welsh, Norwegian, Esperanto or English. |
</div>
